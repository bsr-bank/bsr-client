package app.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class AccountConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("account.wsdl");
        return marshaller;
    }

    @Bean
    public AccountClient countryClient(Jaxb2Marshaller marshaller) {
        AccountClient client = new AccountClient();
        client.setDefaultUri("http://localhost:8080/ws/account.wsdl");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
