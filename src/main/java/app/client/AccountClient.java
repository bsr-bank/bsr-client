package app.client;

import account.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class AccountClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(AccountClient.class);

    public GetBalanceResponse getBalance(String nrb) {
        GetBalanceRequest request = new GetBalanceRequest();
        request.setNrb(nrb);
        log.info("Requesting for nrb: " + nrb);
        GetBalanceResponse response = (GetBalanceResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }

    public DepositResponse deposit(String nrb, int amount) {
        DepositRequest request = new DepositRequest();
        request.setNrb(nrb);
        request.setAmount(amount);
        log.info("Deposit request - nrb: " + nrb + "; amount: " + Integer.toString(amount) );
        DepositResponse response = (DepositResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }

    public WithdrawalResponse withdrawal(String nrb, int amount) {
        WithdrawalRequest request = new WithdrawalRequest();
        request.setNrb(nrb);
        request.setAmount(amount);
        log.info("Withdrawal request - nrb: " + nrb + "; amount: " + Integer.toString(amount) );
        WithdrawalResponse response = (WithdrawalResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }

    public GetAccountsResponse getAccounts(String username) {
        GetAccountsRequest request = new GetAccountsRequest();
        request.setUsername(username);
        GetAccountsResponse response = (GetAccountsResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }

    public TransferResponse transfer(Transfer transfer) {
        TransferRequest request = new TransferRequest();
        request.setTransfer(transfer);
        TransferResponse response = (TransferResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }

    public GetHistoryResponse getHistory(String nrb) {
        GetHistoryRequest request = new GetHistoryRequest();
        request.setNrb(nrb);
        GetHistoryResponse  response = (GetHistoryResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }

    public LoginResponse authenticate(String username, String password) {
        LoginRequest request = new LoginRequest();
        request.setUsername(username);
        request.setPassword(password);
        LoginResponse response = (LoginResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8080/ws/account", request);
        return response;
    }
}
