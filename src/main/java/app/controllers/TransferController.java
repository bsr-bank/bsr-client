package app.controllers;

import account.wsdl.Transfer;
import app.client.AccountClient;
import app.helpers.CurrentNrb;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.client.SoapFaultClientException;

@Component
public class TransferController {

    @Autowired
    private AccountClient accountClient;

    public TextField amountTextField;
    public TextField destinationAccountTextField;
    public TextField destinationNameTextField;
    public TextField titleTextField;

    public void handleSubmitButton(ActionEvent actionEvent) {
        if (verfifyForm()) {
            try {
                int amount = Integer.parseInt(amountTextField.getText());
                Transfer transfer = new Transfer();
                transfer.setAmount(amount);
                transfer.setDestinationAccount(destinationAccountTextField.getText());
                transfer.setDestinationName(destinationNameTextField.getText());
                transfer.setTitle(destinationNameTextField.getText());
                transfer.setSourceAccount(CurrentNrb.getInstance().getNrb());
                transfer.setSourceName("John Doe");
                accountClient.transfer(transfer);
                clearForm();
            } catch (NumberFormatException e) {
                showAlert("Niepoprawna kwota.");
            } catch (SoapFaultClientException fault) {
                showAlert(fault.getFaultStringOrReason());
            }
        } else {
            showAlert("Puste pola!");
        }
    }

    private Boolean verfifyForm() {
        return !amountTextField.getText().isEmpty() &&
                !destinationAccountTextField.getText().isEmpty() &&
                !destinationNameTextField.getText().isEmpty() &&
                !titleTextField.getText().isEmpty();
    }

    private void showAlert(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR, msg, ButtonType.CLOSE);
        alert.showAndWait();
    }

    private void clearForm() {
        amountTextField.clear();
        destinationAccountTextField.clear();
        destinationNameTextField.clear();
        titleTextField.clear();
    }
}
