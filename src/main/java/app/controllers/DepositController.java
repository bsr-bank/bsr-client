package app.controllers;

import account.wsdl.DepositResponse;
import app.client.AccountClient;
import app.helpers.CurrentNrb;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.client.SoapFaultClientException;

@Component
public class DepositController {

    private enum OperationType {
        DEPOSIT, WITHDRAWAL
    }

    @Autowired
    private AccountClient accountClient;

    public RadioButton depositRadio;
    public RadioButton withdrawalRadio;

    public TextField amountTextField;


    public void handleSubmitButton(ActionEvent actionEvent) {
        if (!CurrentNrb.getInstance().getNrb().isEmpty() && !amountTextField.getText().isEmpty()) {
            try {
                int amount = Integer.parseInt(amountTextField.getText());
                if (amount > 0) {
                    if (getOperationType() == OperationType.DEPOSIT) {
                        accountClient.deposit(CurrentNrb.getInstance().getNrb(), amount);
                    } else {
                        accountClient.withdrawal(CurrentNrb.getInstance().getNrb(), amount);
                    }
                    clearForm();
                } else {
                    showAlert("Kwota musi być dodatnia.");
                }
            } catch (NumberFormatException e) {
                showAlert("Niepoprawna kwota.");
            } catch (SoapFaultClientException fault) {
                showAlert(fault.getFaultStringOrReason());
            }
        }
    }

    private void clearForm() {
        amountTextField.clear();
    }

    private OperationType getOperationType() {
        if (depositRadio.isSelected()) {
            return OperationType.DEPOSIT;
        } else {
            return OperationType.WITHDRAWAL;
        }
    }

    private void showAlert(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR, msg, ButtonType.CLOSE);
        alert.showAndWait();
    }
}
