package app.controllers;

import account.wsdl.History;
import app.client.AccountClient;
import app.helpers.CurrentNrb;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HistoryController {

    @Autowired
    private AccountClient accountClient;

    public TextArea historyLogTextArea;


    private void populateLog() {
        List<History> histories = accountClient.getHistory(CurrentNrb.getInstance().getNrb()).getHistories();
        String log = "";
        for (History history : histories) {
            log += history.toString() + "\n";
        }
        historyLogTextArea.setText(log);
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        populateLog();
    }
}
