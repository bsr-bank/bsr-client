package app.controllers;

import account.wsdl.LoginResponse;
import app.client.AccountClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.client.SoapFaultClientException;

import java.io.IOException;

@Component
public class LoginController {
    @Autowired
    private ConfigurableApplicationContext context;
    @Autowired
    private AccountClient accountClient;

    public TextField usernameTextField;
    public TextField passwordTextField;

    public void handleLoginButton(ActionEvent actionEvent) throws IOException {
        if (verifyForm()) {
            try {
                LoginResponse response = accountClient.authenticate(usernameTextField.getText(), passwordTextField.getText());
                if (!response.getUserId().isEmpty()) {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/root.fxml"));
                    loader.setControllerFactory(context::getBean);
                    Parent root = loader.load();
                    RootController destinationController = loader.<RootController>getController();
                    destinationController.setUsername(usernameTextField.getText());
                    Stage stage = new Stage();
                    stage.setTitle("Bank client");
                    stage.setScene(new Scene(root, 770, 400));
                    stage.show();
                    ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
                }
            } catch (SoapFaultClientException fault) {
                showAlert(fault.getFaultStringOrReason());
            }
        }
    }

    private Boolean verifyForm() {
        return !usernameTextField.getText().isEmpty() &&
                !passwordTextField.getText().isEmpty();
    }

    private void showAlert(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR, msg, ButtonType.CLOSE);
        alert.showAndWait();
    }
}
