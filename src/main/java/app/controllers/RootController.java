package app.controllers;

import account.wsdl.GetAccountsResponse;
import account.wsdl.GetBalanceResponse;
import app.client.AccountClient;
import app.helpers.CurrentNrb;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class RootController {


    @Autowired
    private AccountClient accountClient;

    @FXML
    public ChoiceBox choiceBox;
    @FXML
    public Label balanceLabel;
    @FXML
    public Label usernameLabel;

    private String username;

    public void setUsername(String username) {
        this.username = username;
        populateChoiceBox();
    }

//    @FXML
//    public void initialize() {
//        populateChoiceBox();
//    }

    public void handleGetBalanceButton(ActionEvent actionEvent) {
        GetBalanceResponse response = accountClient.getBalance(choiceBox.getValue().toString());
        int balance = response.getBalance();
        balanceLabel.setText("Stan konta: " + Double.toString(balance/100.0) + " PLN");
    }

    private void populateChoiceBox() {
        String tmpUsername = username;    //TODO: Username from login
        usernameLabel.setText("Użytkownik:\t " + username);
        GetAccountsResponse response = accountClient.getAccounts(username);
        if (!response.getNrbs().isEmpty()) {
            choiceBox.setItems(FXCollections.observableList(response.getNrbs()));
            choiceBox.setValue(response.getNrbs().get(0));
            CurrentNrb.getInstance().setNrb(response.getNrbs().get(0));
        }
    }

    public void handleChoiceChange(ActionEvent actionEvent) {
        CurrentNrb.getInstance().setNrb(choiceBox.getValue().toString());
        handleGetBalanceButton(actionEvent);
    }
}
