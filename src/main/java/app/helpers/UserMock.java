package app.helpers;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class UserMock {

    private static String username = "John Doe";
    private static String nrb = "0011111111222222222222222";

    private static UserMock ourInstance = new UserMock();

    public static UserMock getInstance() {
        return ourInstance;
    }

    private UserMock() {
    }
}
