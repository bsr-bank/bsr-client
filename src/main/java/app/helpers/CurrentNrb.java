package app.helpers;

public class CurrentNrb {

    private static CurrentNrb ourInstance = new CurrentNrb();

    private String nrb;

    public static CurrentNrb getInstance() {
        return ourInstance;
    }

    private CurrentNrb() {
    }

    public String getNrb() {
        return nrb;
    }

    public void setNrb(String nrb) {
        this.nrb = nrb;
    }
}
